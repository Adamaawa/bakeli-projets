export class Book {
    constructor(title, author, description, pages, currentPage, read) {
        this.title = title;
        this.author = author;
        this.description = description;
        this.pages = pages;
        this.currentPage = 1;
        this.read = false;
    }
    readBook(page) {
        if (page < 1 || page > this.pages) {
            return  0;
 
        } else if (page >= 1 && page < this.pages) {
            this.currentPage = page;
            return  1;
 
        } else if (page === this.pages) {
            this.currentPage = page;
            this.read = true ;
            return  1;
       }
 
    }

};
let firstBook1 = new Book(
    "TOME 1 ",
    "Les Contemplations",
    "Longue descritpion",
    324,
    0,
    false,
);
 
let secondBook2 = new Book(
    "Tome 2",
    "Les Contemplations",
    "Courte descritpion",
    436,
    0,
    false,
);
 
let thirdBook3 = new Book(
    "Tome 1",
    "Harryy Potter à l'école des sorciers",
    "Moyenne descritpion",
    308,
    0,
    false,
);

export const books = [firstBook1, secondBook2, thirdBook3];
